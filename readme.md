# Material for BIO-511 lecture

## Order of notebooks

Notebook are meant to be explored in this order. Italic text indicates optional material.

1. BIO-511_networks_tutorial
2. BIO-511_networks_exercise
3. *BIO-511_networks_misleading_figures*
4. *BIO-511_networks_analysis*
5. *BIO-511_centrality_figures*
6. BIO-511_proteins_exercise

## Explore online

This repository is on Binder.
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/aliceschwarze%2Fbio551_lecture_material/master)